export default class Starship {
  constructor(name, consumables, passengers) {
    this.name = name;
    if (consumables.split(" ")[1][0] === "y") {
      this._consumables = Number(consumables.split(" ")[0]) * 365; // should be parsed to number as days;
    } else if (consumables.split(" ")[1][0] === "m") {
      this._consumables = Number(consumables.split(" ")[0]) * 30;
    } else if (consumables.split(" ")[1][0] === "w") {
      this._consumables = Number(consumables.split(" ")[0]) * 7;
    } else this._consumables = Number(consumables.split(" ")[0]);
    this._passengers = Number(passengers.split(",").join("")); //should be parsed to number;
  }
 get maxDaysInSpace() {
    return (this._consumables / this._passengers);
 }
 
}
