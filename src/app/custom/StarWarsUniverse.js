import Starships from "./Starship";

export default class StarWarsUniverse {
  constructor() {
    this.starships = [];
  }
  async init() {
    //each ship data wich is valid init new Starship;
    // all initializated starships should be added in this.starships;
    const allShips = await this._createStarships();
    allShips.forEach((ship) => {
      if (this._validateData(ship)) {
        const entity = new Starships(
          ship.name,
          ship.consumables,
          ship.passengers
        );
        // print(entity);
        this.starships.push(entity);
      }
    });
    // this.starships.length = 11;
    return this.starShips;
    
  }
  async _getStarshipCount() {
    const response = await fetch("https://swapi.booost.bg/api/starships/");
    const starShips = await response.json();
    return starShips.count;
  }
  async _createStarships() {
    //fetch data for each individual starship;
    //1-36;
    let ships = [];
    let count = 1;
    while (count < 5) {
      const response = await fetch(
        `https://swapi.booost.bg/api/starships?page=${count}`
      );
      const starShips = await response.json();
      count++;

      for (const ship of starShips.results) {
        ships.push(ship);
      }
    }
    return ships;
  }

  _validateData(ship) {
    //each ship is not undefined;
    //property passenger is not undefined;
    if (
      ship.consumables.split(" ")[1] !== undefined &&
      Number(ship.passengers.split(",").join(""))
    ) {
      return true;
    } else {
      return false;
    }
  }

  get theBestStarship() {
    let theBestStarshipScore = 0;
    let theBestStarship;
    for (const ship of starShips) {
      if (ship.maxDaysinSpace() > theBestStarshipScore) {
        theBestStarshipScore = ship.maxDaysinSpace();
        theBestStarship = ship;
      }
      return theBestStarship;
  //    let ship = new Starships('Millennium Falcon',"2 m", "6")
  //  return ship;
    }
  }
}
